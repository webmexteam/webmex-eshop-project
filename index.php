<?php
// 1) PHP check
version_compare(PHP_VERSION, '5.3', '<') && exit('Webmex requires PHP 5.3 or newer.');


// 2) Load all PHP libraries
require __DIR__ . '/vendor/autoload.php';


// 3) Setup Nette\Tracy
use Tracy\Debugger;

$debugMode = Debugger::DETECT;
if ( in_array($_SERVER['REMOTE_ADDR'], array(
	// developers IP adresses here
'37.157.196.130', // test server
'10.0.2.2'	    // Tomáš Polešovský
	)) 
) {
	$debugMode = Debugger::DEVELOPMENT;
}
if (@$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
	$debugMode = Debugger::PRODUCTION;
}

Debugger::enable($debugMode, __DIR__ . '/app/log' /*, 'errors@webmex.cz'*/);


// 4) Setup & run application
// load individual e-shop core classes (not recomended)
//require_once(__DIR__ . '/app/override/core.php');

Core::run(
	array(                        // $configPaths
		__DIR__ . '/app/def.php',
		//__DIR__ . '/app/gopay.php',
	),
	__DIR__ . '/app/templates',   // $templateDir
	__DIR__ . '/app/tmp',         // $tempDir
	__DIR__ . '/app/log',         // $logDir
	__DIR__ . '/app/xml_import',  // $xmlImportDir
	__DIR__ . '/app/modules'      // $modulesDir
);
